import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KerelafloodreliefComponent } from './kerelafloodrelief.component';

describe('KerelafloodreliefComponent', () => {
  let component: KerelafloodreliefComponent;
  let fixture: ComponentFixture<KerelafloodreliefComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KerelafloodreliefComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KerelafloodreliefComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
